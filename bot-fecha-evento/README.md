## Como Executar o código

### 1. Crie um ambiente de desenvolvimento

```
python3 -m venv .venv
source .venv/bin/activate
```

### 2. Instale as dependencias

```
pip install -r requirements.txt
```

### 3. Abra o google chrome pelo terminal

```
google-chrome --remote-debugging-port=9222 --user-data-dir=/tmp/someProfileDir
```

### 4. Faça login no Brasil Participativo com sua conta do Gov

### 5. Execute o script

```
python main.py
```

### 6. Acompanhe as saidas no arquivo de log