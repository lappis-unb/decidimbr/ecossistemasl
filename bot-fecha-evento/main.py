from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from datetime import datetime
import time
import logging

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s - %(levelname)s - %(message)s",
    filename=f"execute{time.time()}.log",
)


class EventCloser:
    def __init__(self, base_url, component_id, process_slug, start_page=1):
        self.base_url = base_url
        self.component_id = component_id
        self.process_slug = process_slug
        self.current_page = start_page

        options = webdriver.ChromeOptions()
        options.debugger_address = "localhost:9222"

        self.driver = webdriver.Chrome(options=options)

        self.wait = WebDriverWait(self.driver, 10)

    def run(self):
        logging.info("Starting event closer bot")
        while True:
            logging.info("=" * 20)
            page_url = self._build_page_url(self.current_page)
            if not self._page_exists(page_url):
                break

            self.driver.get(page_url)
            links = self._get_clickable_links()

            for link in links:
                self._process_link(link)
                time.sleep(1)

            self.current_page += 1

        self.driver.quit()

    def _build_page_url(self, page_number):
        logging.info(f"Building page URL for page {page_number}")
        return f"{self.base_url}/admin/participatory_processes/{self.process_slug}/components/{self.component_id}/manage/meetings?component_id={self.component_id}&page={page_number}&participatory_process_slug={self.process_slug}"

    def _page_exists(self, url):
        logging.info(f"Checking if page exists: {url}")
        try:
            self.driver.get(url)
            self.wait.until(
                EC.presence_of_element_located((By.CLASS_NAME, "table-list"))
            )
            rows = self.driver.find_elements(By.CSS_SELECTOR, "tr[data-id]")

            if len(rows) == 0:
                logging.info("Last page reached")
                return False
            return True
        except:
            logging.info("Error while checking page existence")
            return False

    def _get_clickable_links(self):
        logging.info("Getting clickable links")
        rows = self.driver.find_elements(By.CSS_SELECTOR, "tr[data-id]")
        clickable = []

        months_map = {
            "janeiro": 1,
            "fevereiro": 2,
            "março": 3,
            "abril": 4,
            "maio": 5,
            "junho": 6,
            "julho": 7,
            "agosto": 8,
            "setembro": 9,
            "outubro": 10,
            "novembro": 11,
            "dezembro": 12,
        }

        for row in rows:
            tds = row.find_elements(By.TAG_NAME, "td")

            status_text = tds[4].text.strip()
            end_date_str = tds[3].text.strip()

            # Only proceed if status is "Não"
            if status_text == "Não":
                if end_date_str:
                    # Example of format: "janeiro 23, 2025 12:00"
                    # Expected split: ["janeiro", "23,", "2025", "12:00"]
                    parts = end_date_str.split()
                    if len(parts) == 4:
                        month_name = parts[0].lower()
                        day_str = parts[1].replace(",", "")
                        year_str = parts[2]
                        time_str = parts[3]

                        hour_str, minute_str = time_str.split(":")

                        day = int(day_str)
                        year = int(year_str)
                        hour = int(hour_str)
                        minute = int(minute_str)

                        month = months_map.get(month_name, None)

                        if month is not None:
                            event_end_date = datetime(year, month, day, hour, minute)

                            # Check if event_end_date is in the past
                            if event_end_date < datetime.now():
                                link = row.find_element(
                                    By.CSS_SELECTOR, "a.action-icon.action-icon--close"
                                )
                                clickable.append(link.get_attribute("href"))
                else:
                    pass

        return clickable

    def _process_link(self, link_url):
        logging.info(f"Processing link: {link_url}")
        self._open_link(link_url)

        # Insert text into editor
        editor = self.wait.until(
            EC.presence_of_element_located((By.CSS_SELECTOR, ".ql-editor"))
        )
        self._insert_text_in_editor(
            editor,
            "Etapa automaticamente finalizada pela plataforma Brasil Participativo.",
        )

        # Insert number into input
        attendees_input = self.wait.until(
            EC.presence_of_element_located((By.ID, "close_meeting_attendees_count"))
        )
        attendees_input.clear()
        attendees_input.send_keys("1")

        # Click submit button
        close_button = self.wait.until(
            EC.element_to_be_clickable(
                (By.CSS_SELECTOR, "button.button[type='submit']")
            )
        )
        close_button.click()

        time.sleep(2)

    def _open_link(self, url):
        logging.info(f"Opening link: {url}")
        self.driver.get(url)

    def _insert_text_in_editor(self, editor_element, text):
        editor_element.click()
        editor_element.send_keys(text)


if __name__ == "__main__":
    base_url = "https://brasilparticipativo.presidencia.gov.br"
    component_id = 130
    process_slug = "cnma"
    start_page = 1

    event_closer = EventCloser(base_url, component_id, process_slug, start_page)
    event_closer.run()
